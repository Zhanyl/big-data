package com.lab.bytecount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;



public class AppTest {

    MapDriver<Object, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, Text> reduceDriver;
  //  MapReduceDriver<Object, Text, Text, IntWritable, Text, Text> mapReduceDriver;



    @Before
    public void setUp() {
        App.ByteMapper mapper = new App.ByteMapper();
        App.IntSumReducer reducer = new App.IntSumReducer();


        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
//        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);

    }


    @Test
/**
 * default Mapper
 * */
    public void testMapperDefault() throws Exception {

        String inputString = "106.54.31.193 - - [18/Oct/2017:21:28:42 +0300] \"GET /posts/posts/explore HTTP/1.0\" 200 4997 \"http://www.james-smith.com/categories/index/\" \"Mozilla/5.0 (Windows NT 5.2; it-IT; rv:1.9.0.20) Gecko/2010-09-01 22:06:03 Firefox/3.6.17\"";
        mapDriver.withInput(new LongWritable(), new Text(inputString));
        mapDriver.withOutput(new Text("106.54.31.193"), new IntWritable(4997));

        mapDriver.runTest();

    }

    @Test
/**
 * default Mapper
 * */
    public void testMapperIncorrectLogEntry() throws Exception {

        String inputString = "106.54.31.193 - - [18/Oct/2017:21:28:42 +0300] \"GET /posts/posts/explore HTTP/1.0\" 200 ";
        mapDriver.withInput(new LongWritable(), new Text(inputString));
//        mapDriver.withOutput(new Text(" "), new IntWritable(0));
        mapDriver.runTest();
        assertEquals("Expected INCORRECT_LOG_ENTRY counter to increment",1,
                mapDriver.getCounters().findCounter(CustomCounter.INCORRECT_LOG_ENTRY).getValue());

    }



    @Test
/**
 * default reducer
 * */
    public void testReducerDefault() throws Exception {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(4997));
        values.add(new IntWritable(500));
        reduceDriver.withInput(new Text("106.54.31.193"), values);
        reduceDriver.withOutput(new Text("106.54.31.193"), new Text(String.valueOf(2748.5) + " "
                                                                                            +String.valueOf(5497)));

        reduceDriver.runTest();

    }



}
