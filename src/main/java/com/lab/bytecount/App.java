package com.lab.bytecount;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
//main map_reduce class
public class App {

/*
*
*Custom Mapper class to count bytes per http request
*
*/
  public static class ByteMapper
       extends Mapper<Object, Text, Text, IntWritable>{


    private Text word = new Text();
    private final static String regexp = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\"";
    private final static Pattern pat = Pattern.compile(regexp);

    /**
     *
     *  Map method
     *
     * */
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      
      Matcher matcher = pat.matcher(value.toString());
      // use regular expression to parse log entry
        // if matcher fails - do nothing
      if (!matcher.matches()){
          context.getCounter(CustomCounter.INCORRECT_LOG_ENTRY).increment(1);
//          ip = " ";
//          bytes = 0;
      }
      else{
          //else  - create new (ip, bytes) pair (ip - 1 position; bytes - 7 position)
        String ip;
        int bytes=0;
        ip=matcher.group(1);
        bytes=Integer.parseInt(matcher.group(7));
        word.set(ip);
        context.write(word, new IntWritable(bytes));
      }


    }
  }

  public static class IntSumReducer
       extends Reducer<Text,IntWritable,Text,Text> {
    private Text result = new Text();
/**
 * reduce method
 * counts total number of bytes per IP and average number of bytes per request
 *
 * */
    public void reduce(Text key, Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      int sum = 0;
      int numRequest = 0;
      if(key.toString()==" "){
        context.write(new Text("bad IP"),new  Text("0 0"));
      }
      else {
        for (IntWritable val : values) {
          sum += val.get();
          numRequest++;
        }
        //this task required to output data like IP avgBytesPerRequest allbytes
        //to do so, it outputs both key and value as Text type
        String interRes = String.valueOf((float) sum / (float) numRequest) + " " + String.valueOf(sum);
        result.set(interRes);
        context.write(key, result);
      }
    }
  }


  /**
   *
   * entry point
   * */
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "Byte count");
    job.setJarByClass(App.class);
    job.setMapperClass(ByteMapper.class);

    job.setReducerClass(IntSumReducer.class);
    job.setOutputKeyClass(Text.class);
    //set outputfile format as sequence file
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    job.setOutputValueClass(Text.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    SequenceFileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}
